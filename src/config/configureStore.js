import { createStore, combineReducers } from 'redux';
import reducers from '../store/reducers/adminReducers';

export default function configureStore() {
  return createStore(
    combineReducers({
      ...reducers
    }),
    {}
  );
}
