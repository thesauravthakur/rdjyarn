import React, { Fragment } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  Avatar,
  Box,
  Menu,
  Button,
  List,
  ListItem,
  Tooltip,
  Divider
} from '@material-ui/core';

export default function HeaderUserbox() {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Fragment>
      <Button
        color="inherit"
        onClick={handleClick}
        className="text-capitalize px-3 text-left btn-inverse d-flex align-items-center">
        <Box>
          <Avatar sizes="44" alt="Emma Taylor" src='https://scontent.fktm8-1.fna.fbcdn.net/v/t1.0-1/p200x200/67149060_517212019034074_2706303144257650688_n.jpg?_nc_cat=109&_nc_sid=7206a8&_nc_oc=AQliYZzbIOzXWyP6xDiwVgtBpd3t2r2Lm1JjWK7yxA5AV--bi8cAA6G241aIjwpyuI8&_nc_ht=scontent.fktm8-1.fna&_nc_tp=6&oh=fdecade0dbd6fc8c57a13dd39c171b5f&oe=5F20DE58' />
        </Box>
        <div className="d-none d-xl-block pl-3">
          <div className="font-weight-bold pt-2 line-height-1">Ryan Kent</div>
          <span className="text-white-50">Senior React Developer</span>
        </div>
        <span className="pl-1 pl-xl-3">
          <FontAwesomeIcon icon={['fas', 'angle-down']} className="opacity-5" />
        </span>
      </Button>

      <Menu
        anchorEl={anchorEl}
        keepMounted
        getContentAnchorEl={null}
        open={Boolean(anchorEl)}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'center'
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'center'
        }}
        onClose={handleClose}
        className="ml-2">
        <div className="dropdown-menu-right dropdown-menu-lg overflow-hidden p-0">
          <List className="text-left bg-transparent d-flex align-items-center flex-column pt-0">
            <Box>
              <Avatar sizes="44" alt="Emma Taylor" src='https://scontent.fktm8-1.fna.fbcdn.net/v/t1.0-1/p200x200/67149060_517212019034074_2706303144257650688_n.jpg?_nc_cat=109&_nc_sid=7206a8&_nc_oc=AQliYZzbIOzXWyP6xDiwVgtBpd3t2r2Lm1JjWK7yxA5AV--bi8cAA6G241aIjwpyuI8&_nc_ht=scontent.fktm8-1.fna&_nc_tp=6&oh=fdecade0dbd6fc8c57a13dd39c171b5f&oe=5F20DE58' />
            </Box>
            <div className="pl-3  pr-3">
              <div className="font-weight-bold text-center pt-2 line-height-1">
                Saurav
              </div>
              <span className="text-black-50 text-center">
                Developer
              </span>
            </div>
            <Divider className="w-100 mt-2" />
            <ListItem button>My Account</ListItem>
            <Divider className="w-100" />
            <ListItem className="d-block rounded-bottom px-3 pt-3 pb-0 text-center">
              <Tooltip arrow title="Twitter">
                <a target="_blank" href='https://www.facebook.com/iamthakursaurav'>
                  <Button color="default" className="text-twitter">
                    <span className="btn-wrapper--icon">
                      <FontAwesomeIcon icon={['fab', 'facebook']} />
                    </span>
                  </Button>
                </a>
              </Tooltip>
            </ListItem>
          </List>
        </div>
      </Menu>
    </Fragment>
  );
}
