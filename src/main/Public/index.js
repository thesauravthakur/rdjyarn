import React from 'react';
import PresentationLayout from 'main/PresentationLayout';

class PublicIndex extends React.Component {
    render() {
        return (
            <PresentationLayout>
                <p>this is Public Index</p>
                {this.props.children}
            </PresentationLayout>
        )
    }
}
export default PublicIndex;