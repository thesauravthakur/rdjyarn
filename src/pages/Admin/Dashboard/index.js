import React, { Fragment } from 'react';

import { PageTitle } from '../../../layout/Admin';

import Dashboard from '../../../components/Admin/Dashboard';

export default function DashboardDefault() {
  return (
    <Fragment>
      <PageTitle
        titleHeading="Default"
        titleDescription="This is a dashboard page example built using this template."
      />

      <Dashboard />
    </Fragment>
  );
}
