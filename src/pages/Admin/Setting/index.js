import React, { Fragment } from 'react';

import { PageTitle } from '../../../layout/Admin';

import { WrapperSimple } from '../../../layout/Admin';

export default function Setting() {
  return (
    <Fragment>
      <PageTitle
        titleHeading="Setting"
        titleDescription="This is setting section, Here you can change the settings."
      />
      <WrapperSimple sectionHeading="Settings">
        Comming soon...
      </WrapperSimple>

    </Fragment>
  );
}
