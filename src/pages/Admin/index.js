export { default as DashboardDefault } from './Dashboard';
export { default as Buttons } from './Setting';
export { default as Badges } from './Setting';
export { default as LogInPage } from './LoginPage';
